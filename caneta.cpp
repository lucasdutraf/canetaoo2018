#include "caneta.hpp"
#include <iostream>

Caneta::Caneta(){
   std::cout << "Construtor da Caneta" << std::endl;
   cor = "Branca";
   marca = "Genérica";
   preco = 1.0;
}

Caneta::~Caneta(){
   std::cout << "Destrutor da Caneta" << std::endl;
}

void Caneta::setCor(std::string cor){
   this->cor = cor;
}

std::string Caneta::getCor(){
   return cor;
}

void Caneta::setMarca(std::string marca){
   this->marca = marca;
}

std::string Caneta::getMarca(){
   return marca;
}

void Caneta::setPreco(float preco){
   this->preco = preco;
}

float Caneta::getPreco(){
   return preco;
}

void Caneta::imprimeDados(){
   std::cout << "Cor: " << cor << std::endl;
   std::cout << "Marca: " << marca << std::endl;
   std::cout << "Preço: " << preco << std::endl;
}
